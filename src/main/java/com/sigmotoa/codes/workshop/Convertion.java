package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

//Km to metters
    public static int kmToM1(double km)
    {
        int meter = 1000;
        int kmInt = (int) km;
        return kmInt * meter;
    }

//Km to metters
    public static double kmTom(double km)
    {
        double kmToMetter = 1000;
        return km * kmToMetter;
    }

    //Km to cm
    public static double kmTocm(double km)

    { double kmToCm = 100000;
        return km * kmToCm;
    }

    //milimetters to metters
    public static double mmTom(int mm)
    {
        double mmToM = 0.001;
        return mm * mmToM;

    }
//convert of units of U.S Standard System

//convert miles to foot
    public static double milesToFoot(double miles)
    {
        double milesToFoot = 5280;
        return miles *  milesToFoot;

    }

//convert yards to inches
    public static int yardToInch(int yard)
    {
        int yardToInch = 36;
        return yard * yardToInch;

    }
    
    //convert inches to miles
    public static double inchToMiles(double inch)
    {
        double inchToMile = 1.5782828E-5;
        return inch * inchToMile;

    }
//convert foot to yards
    public static int footToYard(int foot)
    {
        double footToYard = 0.333333;
        return (int) ((int)foot * footToYard);

    }

//Convert units in both systems

//convert Km to inches
    public static double kmToInch(String km)
    {double kmToInch = 39370.1;
        return Math.ceil(Double.parseDouble(km) * kmToInch);
    }

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {double mmToFoot = 0.00328084;
        return Double.parseDouble(mm) * mmToFoot;
    }
//convert yards to cm    
    public static double yardToCm(String yard)
    {double yardToCm = 91.44;
        return Double.parseDouble(yard) * yardToCm;
    }


}
